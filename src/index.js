import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {initStore} from 'Redux/store'
import {setPhase} from 'Redux/actions'
import View from 'view'
import {loadData} from 'data'

const store = initStore() //bind reducers and actions to Redux store
const rootElement = document.getElementById('root')

loadData('https://randomuser.me/api/?nat=gb&results=5')
    .then((data) => {
        setPhase(data.map(item => item.id), 'applied')
        ReactDOM.render(
                <Provider store={store}>
                    <View />
                </Provider>,
           rootElement 
        )
    })
    .catch((e) => {
        ReactDOM.render(
            <h1> Network error occured </h1>,
            rootElement
        )
    })
        
