import {combineReducers} from 'redux'
import phasesReducer from 'phases/reducers'
import filtersReducer from 'filters/reducers'

export default combineReducers({
    phases: phasesReducer,
    filters: filtersReducer,
})
