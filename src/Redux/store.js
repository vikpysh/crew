import {createStore, applyMiddleware, compose} from 'redux'
import {assignAll} from 'redux-act'
import * as actions from './actions'
import rootReducer from './reducers'
import middleware from './middleware'



function configureStore() {
    const store = compose(
        applyMiddleware(...middleware)
    )(createStore)(rootReducer)

    return store
}


export function initStore() {
    const store = configureStore()
    assignAll(actions, store)
    return store
}
