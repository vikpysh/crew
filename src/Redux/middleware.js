import {createLogger} from 'redux-logger'

const middleware = []
middleware.push(createLogger())

export default middleware
