let Data

const handleResponse = (data) => {
    Data = data.results.map(item =>{
        return {
            id: item.id.value,
            name: `${item.name.first} ${item.name.last}`,
            photoUrl: item.picture.thumbnail,
            city: `${item.location.city}`
        }
    })
    return Data
}
export const loadData = (url) => {
    return fetch(url).then(response => {
        return response.json()
    }).then(handleResponse)
}

export const getData = () => Data
export const getItem = (id) => Data.find(item => item.id == id)
