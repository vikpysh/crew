import {createReducer} from 'redux-act'
import {setFilterValue} from './actions'

const initialState = {
    name: '',
    city: '',
}

export default createReducer({
    [setFilterValue]: (state, {name, value}) => {
        return {...state, [name]: value}
    },
}, initialState)
