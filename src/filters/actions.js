import { createAction } from 'redux-act'

export const setFilterValue = createAction('SET_FILTER_VALUE', (name, value) => ({name, value}))
