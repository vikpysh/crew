import React from 'react'
import ReactDOM from 'react-dom'
import {connect} from 'react-redux'
import {getData} from 'phases/selectors'

const GridContext = React.createContext()

    
const Cell = ({children, isHead}) => {
    return (
      <div className="cell">
        <GridContext.Consumer>
            {({CellView, HeadView}) => (
                isHead ? <HeadView>{children}</HeadView> :
                 <CellView>{children}</CellView>
            )}
        </GridContext.Consumer>
      </div>
   )
}

const Column = ({items}) => (
    <div className="column">
        {items.map((item, index) => {
            return (<Cell {...{key: item, isHead: index === 0}} >{item}</Cell>)
        })}
    </div>
)


const Grid = ({data, CellView, HeadView}) => {
    return (
      <GridContext.Provider value={{CellView, HeadView}}>
        <div className="grid">
            {data.map((columnData, index) => {
                return (<Column {...{key: index, items: columnData}} />)
            })}
        </div>
      </GridContext.Provider>
    )
}
        
const mapStateToProps = (state) => {
    return {
        data: getData(state),
    }
}

export default connect(mapStateToProps)(Grid)
