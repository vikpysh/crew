import React from 'react'
import Filters from 'view/Filters'
import Grid from 'view/Grid'
import Card from 'view/Card'
import Head from 'view/Head'

const View = () => (
    <div className="wrapper">
        <Filters />
        <Grid CellView={Card} HeadView={Head}/>
    </div>
)

export default View
