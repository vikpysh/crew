import React from 'react'
import {connect} from 'react-redux'
import {moveForward, moveBack} from 'phases/actions'
import {isFirstPhase, isLastPhase} from 'phases/selectors'
import {getItem} from 'data'

const onMoveForward = (id) => moveForward(id)
const onMoveBack = (id) => moveBack(id)

const Card = ({id, isFirstPhase, isLastPhase, name, photoUrl}) => (
    <div className="card">
        <div className="first-row">
            <img  className="photo" src={photoUrl} />
            <span className="name">{name}</span>
        </div>
        <div className="nav-button">
            {!isFirstPhase && <button className="left-button" onClick={()=>onMoveBack(id)}>{'\u25c0'}</button>}
            {!isLastPhase && <button className="right-button" onClick={()=>onMoveForward(id)}>{'\u25b6'}</button>}
        </div>
    </div>
)
        
const mapStateToProps = (state, {children}) => {
    const id = children
    const item = getItem(id)
    return {
        id, 
        isFirstPhase: isFirstPhase(state, id),
        isLastPhase: isLastPhase(state, id),
        name: item.name,
        photoUrl: item.photoUrl
    }
}
export default connect(mapStateToProps)(Card)
