import React from 'react'


const Head = ({children}) => (
    <div className="head">
        {children}
    </div>
)
        
export default Head
