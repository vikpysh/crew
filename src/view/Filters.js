import React from 'react'
import {connect} from 'react-redux'
import {setFilterValue} from 'filters/actions'

const Filters = ({value}) => (
    <div className="filters">
        <input  placeholder="Name" value = {value.name} name="name" type="text" onChange={(ev) => setFilterValue('name', ev.target.value)}  />
        <input  placeholder="City" value = {value.city} name="city" type="text" onChange={(ev) => setFilterValue('city', ev.target.value)}  />
    </div>
)

const mapStateToProps = (state) => {
    return {
        value: state.filters,
    }
}

export default connect(mapStateToProps)(Filters)
