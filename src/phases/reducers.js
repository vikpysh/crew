import { createReducer } from 'redux-act'
import {setPhase, moveForward, moveBack} from './actions'
import {getPhaseById} from './selectors'
import {phases, getNextPhase, getPrevPhase} from 'phases/scheme'

const initialState = {}

phases.forEach(item => initialState[item.id] = [])

export default createReducer({
    [setPhase]: (state, {ids, phase}) => {
        return {...state, [phase]: [...ids]}
    },
    [moveForward]: (state, {id}) => {
        const currentPhase = getPhaseById(state, id)
        const nextPhase = getNextPhase(currentPhase)
        if (!nextPhase) {
            return state
        }
        const currentPhaseCollection = [...state[currentPhase]]
        const index = currentPhaseCollection.indexOf(id)
        currentPhaseCollection.splice(index, 1)
        const nextPhaseCollection = [...state[nextPhase]]
        nextPhaseCollection.push(id)
        
        return {...state, [currentPhase]: currentPhaseCollection, [nextPhase]: nextPhaseCollection}
    },
    [moveBack]: (state, {id}) => {
        const currentPhase = getPhaseById(state, id)
        const prevPhase = getPrevPhase(currentPhase)
        if (!prevPhase) {
            return state
        }
        const currentPhaseCollection = [...state[currentPhase]]
        const index = currentPhaseCollection.indexOf(id)
        currentPhaseCollection.splice(index, 1)
        const prevPhaseCollection = [...state[prevPhase]]
        prevPhaseCollection.push(id)
        
        return {...state, [currentPhase]: currentPhaseCollection, [prevPhase]: prevPhaseCollection}
    },
}, initialState)
