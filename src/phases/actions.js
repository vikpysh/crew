import { createAction } from 'redux-act'

export const moveForward = createAction('MOVE_FORWARD', (id) => ({id}))
export const moveBack = createAction('MOVE_BACK', (id) => ({id}))
export const setPhase = createAction('SET_PHASE', (ids, phase) => ({ids, phase}))
