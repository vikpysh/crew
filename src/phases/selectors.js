import {phases, phaseKeys} from './scheme'
import {getItem} from 'data'

export const getData = (state) => {
    const {name, city} = state.filters
    const data = phases.map(item => [
        item.name,
        ...state.phases[item.id]
            .map(id => getItem(id))
            .filter(item => item.name.includes(name.trim()))
            .filter(item => item.city.includes(city.trim()))
            .map(item => item.id)
    ])
    return data
}
    
export const getPhaseById = (state, id) => {
    const [res] = phaseKeys
        .map((key) => ({key, arr: state[key]}))
        .filter(({arr}) => arr.includes(id))
    return res.key
}

export const isFirstPhase = (state, id) => {
    const phase = getPhaseById(state.phases, id)
    return phaseKeys.indexOf(phase) === 0
}
    
export const isLastPhase = (state, id) => {
    const phase = getPhaseById(state.phases, id)
    return phaseKeys.indexOf(phase) === phaseKeys.length - 1
}
    
    
