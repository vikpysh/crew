export const phases = [
    {
        id: 'applied',
        name: 'Applied',
    },
    {
        id: 'interviewing',
        name: 'Interviewing',
    },
    {
        id: 'hired',
        name: 'Hired',
    },
]

export const phaseKeys = phases.map(item => item.id)

export const getNextPhase = (key) => {
    const index = phaseKeys.indexOf(key)
    return phaseKeys[index + 1]
}
export const getPrevPhase = (key) => {
    const index = phaseKeys.indexOf(key)
    return phaseKeys[index - 1]
}


